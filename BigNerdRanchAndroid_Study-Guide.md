# Big Nerd Ranch Programming - Android 


##### Widgets

* Widgets are the building blocks you use to compose a UI. A widget can show text or graphics, interact with the user, or arrange other widgets on the screen. Buttons, text input controls, and checkboxes are all types of widgets


* Widgets exist in a hierarchy of `View` objects called the _view hierarchy_


* Every widget is an instance of the `View` class or one of its subclasses


* A `ViewGroup` is a widget that contains and arranges other widgets


* When a widget is contained by a `ViewGroup`, that widget is said to be a _child_ of the `ViewGroup`  


* Wiring up widgets is a two-step process:
  1. Get references to the inflated `View` objects
  2. Set listeners on those objects to respond to user actions

##### Widget Types

* `LinearLayout`
  * When you want widgets arranged in a single column or row

* `FrameLayout`
  * The simplest `ViewGroup` that does not arrange its children in any particular manner. 
  * Child views are arranged according to their `android:layout_gravity` attribute 

##### XML Elements

* Each element has a set of XML _attributes_. Each _attribute_ is an instruction about how the widget should be configured


* `android:layout_width`
  `android:layout_height`
  * `match_parent` : view will be as big as its parent
  * `wrap_content` : view will be as big as its contents require


* `android:padding`
  * Tells the widget to add the specified amount of space to its contents when determining its size
  * `dp`: density-independent pixels


* `android:orientation`
  * Determines whether their children will appear vertically or horizontally


* `android:text`
  * Tells the widget what text to display
  * _string resources_: A string that lives in a separate XML file called a _strings file_


* `android:id`
  * Generates a resource ID 
  * If there is a `+` sign in the values for `android:id`, this is because you are _creating_ the IDs and only _referencing_ the strings 

##### From Layout XML to View Objects 

* The `onCreate(Bundle)` method is called when an instance of the activity subclass is created


* When an activity is created, it needs a UI to manage. To get the activity its UI, you call the following method:
  * `public void setContentView(int LayoutResID)`
  * This method _inflates_ a layout and puts it on screen
  * When a layout is inflated, each widget in the layout file is instantiated as defined by its attributes 
  * You specify which layout to inflate by passing in the layout's resource ID 

##### Resources and resource IDs

* A `layout` is a _resource_
* A _resource_ is a piece of your application that is not code - things like image files, audio files, and XML files 
* To access a resource in code, you use its _resourceID_


##### Setting Listeners
* Android applications are typically _event driven_. Event-driven applications start and then wait for an event, such as the user pressing a button. 
* When your application is waiting for a specific event, we say it is "listening for" that event. The object that you create to respond to an event is called a _listener_, and the _listener_ implements a _listener interface_ for that event. 
* This listener is implemented as an _anonymous inner class_
  * It puts the implementation of the listeners' methods right where they are being used
  
##### Making Toasts
* A _toast_ is a short message that informs the user of something but does not require any input or action
  * `Toast.makeText(Context context, int resId, int duration)`

---

## Activity Lifecycle

* Every instance of `Activity` has a lifecycle
* During this lifecycle, an activity transitions between four states: resumed, paused, stopped, and nonexistent
  * Nonexistent -> `onCreate()` -> Stopped -> `onStart()` -> Paused -> `onResume()`-> Resumed
  * Resumed -> `onPause()` -> Paused -> `onStop()` -> Stopped -> `onDestroy` -> Nonexistent
* For each transition, there is an `Activity` method that notifies the activity of the change in its state

| State        | In Memory? | Visible to user? |  In Foreground?  
| ------------- |:-------------:| -----:| ---------:|
| nonexistent  | no | no | no |
| stopped |  yes | no | no |
| paused | yes | yes/partially   |  no |
| resumed | yes |  yes | yes  

* Only one activity across all the apps on the device can be in the resumed state at any given time

___

#### Using Logcat
* Logcat: A log viewer included in the Android SDK tools
* Use "Edit Filter Configuration" in Logcat to filter out messages related to appropriate Activity

#####  Log Levels and Methods 
|Log Level | Method | Used For
|---------- |----------|----------------|
ERROR      | Log.e(...)  | errors 
WARNING | Log.w(...) | warnings 
INFO         | Log.i(...)   | informational messages
DEBUG      | Log.d(...) | debug output (may be filtered out)
VERBOSE  | Log.v(...)  | development only

* Each of the logging methods has two signatures: one that takes a `TAG` string and a message string and a second that takes those two arguments plus an instance of `Throwable`, which makes it easy to log information about a particular exception that your application might throw. 

```java
// Log a message at "debug" log level 
Log.d( TAG, "Current question index: " + mCurrentIndex); 
Question question; 
try { 
  question = mQuestionBank[mCurrentIndex]; 
  } catch (ArrayIndexOutOfBoundsException ex) { 
  // Log a message at "error" log level, along with an exception stack trace 
  Log.e(TAG, "Index was out of bounds", ex);
}
```

#### Device Configuration
* The _device configuration_ is a set of characteristics that describe the current state of an individual device 
* The characteristics that make up the configuration include:
  * screen orientation
  * screen density
  * screen size
  * keyboard type
  * dock mode
  * language

* When a _runtime configuration change_ occurs, there may be resources that are a better match for the new configuration. So Android destroys the activity, looks for resources that are the best fit for the new configuration, and then rebuilds a new instance of the acitivity with those resources

#### Saving Data Across Rotation
* One way to save data across a runtime configuration change, like rotation, is to override the `Activity` method:
  * `protected void onSaveInstanceState(Bundle outState)`
    * The default implementation of onSaveInstanceState(Bundle) directs all of the activity's views to save their data in the `Bundle` object
    * A `Bundle` is a structure that maps string keys to values of certain limited types
    * When you override `onCreate(Bundle)`, you call `onCreate(Bundle)` on the activity's superclass and pass in the bundle you just received
    * In the superclass implementation, the saved state of the views is retrieved and used to re-create the activity's view hierarchy
  * You can override `onSaveInstanceState(Bundle)` to save additional data to the bundle and then read that data back in `onCreate(Bundle)`
  * The types that you can save to and restore from a `Bundle` are primitive data types and classes that implement the `Serializable` or `Parcelable` interfaces
  * It is usually bad practice to put objects of custom types into a `Bundle`, however, because the data might be stale when you get it back out

___

## The Activity Lifecycle, Revisited
* An activity can be destroyed by the OS if the user navigates away fror a while and Android needs to reclaim memory
* The OS will not reclaim a visible(paused or resumed) activity. Activities are not marked as "killable" until `onStop()` is called and finishes executing
* How does the data you stash in `onSaveInstanceState(Bundle)` survive the activity's death?
  * When`onSaveInstanceState(Bundle)` is called, the data is saved to the `Bundle` object. That `Bundle` object is then stuffed into your activity's _activity record_ by the OS
* When your activity is stashed, an `Activity` object does not exist, but the activity record object lives on in the OS
* The OS can reanimate the activity using the activity record when it needs to
* Typically, you override `onSaveInstanceState(Bundle)` to stash small, transient-state data that belongs to the current activitry in your `Bundle` 
* Override `onStop()` to save any permanent data, because your activity may be killed at any time after this method returns
* If you are overriding `onSaveInstanceState(Bundle)` you should test that your state is being saved and restored as expected

---

##### Declaring activities in the manifest
* The _manifest_ is an aml file containing metadata that describes your application in the Android OS
* The file is always named AndroidManifest.xml, and it lives in the app/manifests directory of your project
* Every activity in an application must be declared in the manifest so that the OS can access it

##### Starting an Activity
* The simplest way one activity can start another is with the `startActivity` method:
  * `public void startActivity(Intent intent)`
* When an activity calls `startActivity(Intent)`, this call is sent to the OS, specifically to the `ActivityManager`
* The `ActivityManager` then creates the `Activity` instance and calls its `onCreate(Bundle)` method
  * Your activity -> `startActivity(Intent)` -> Android OS / `ActivityManager` -> New Activity

#### Communicating with intents
* An _intent_ is an object that a _component_ can use to communicate with the OS 
* Intents are multipurpose communication tools, and the `Intent` class provides different constructors depending on what you are using the intent to do

#### Explicit and implicit intents
* When you create an `Intent` with a `Context` and a `Class` object, you are creating an _explicit intent_. You use explicit intents to start activities within your application.
* When an activity in your application wants to start an activity in another application, you create an _implcit intent_ 

#### Passing Data Between Activities
* Extras are arbitrary data that the calling activity can include with an intent. 
* The OS forwards the intent to the recipient activity, which can then access the extras and retrieve the data
* An extra is structured as a key-value pair
* To add an extra to an intent, you use `Intent.putExtra(...)` 
* To retrieve the value from the extra, you can use:
  * `public boolean getBooleanExtra(String name, boolean defaultValue)`
* You can put multiple extras on an`Intent` if necessary 
* `Activity.getIntent()` always returns the `Intent` that started the activity. This is what you send when calling `startActivity(Intent)`

#### Getting a result back from a child activity
* When you want to hear back from the child activity, you call the following `Activity` method:
  * `public void startActivityForResult(Intent intent, int requestCode)`
  * The second parameter is the _request code_ 
  * The _request code_ is a user-defined integer that is sent to the child activity and then received back by the parent.
  * It is used when an activity starts more than one type of child activity and needs to know who is reporting back

##### Setting a result
* There are two methods you can call in the child activity to send data back to the parent:
  * `public final void setResult(int resultCode)`
  * `public final void setResult(int resultCode, Intent data)`
* Typically, the _result code_ is one of the two predefined constants: `Activity.RESULT_OK` or `Activity.RESULT_CANCELED` (You can use another constant, `RESULT_FIRST_USER` as an offset when defining your own result codes)
* Setting result codes is useful when the parent needs to take a different action depending on how the child activity finished
* Calling `setResult(....)` is not required of the child activity
* A result code is always returned to the parent if the child activity was started with `startActivityForResult(...)`

##### Sending back an intent
* When the user presses the Back button to return to an `Activity`, the `ActivityManager` calls the following method on the parent activity:
  * `protected void onActivityResult(int requestCode, int resultCode, Intent data)`
* The parameters are the original request code, the result code and intent passed into `setResult(int, Intent)`
* The final step is to override `onActivityResult(int, int, Intent)` to handle the result

## How Android Sees Your Activities 

* _launcher activity_: Specified in the manifest by the intent-filter element in an activities declaration 
`<action android:name="android.intent.action.MAIN" />`
`<category android:name="android.intent.category.LAUNCHER" />`
* `ActivityManager` maintains a _back stack_ and this back stack is not just for your application's activities. Activities for all applications share the back stack, which is one reason the `ActivityManager` is involved in starting your activities and lives with the OS and not your application
* The stack represents the use of the OS and devices as a whole rather than the use of a single application 
___

# Android SDK Versions and Compatibility
## Compatibility and Android Programming 

* SDK Version = API Level
* All of the relevant properties are set in the `build.gradle` file in your app module
* The build version lives exclusively in this file
* The minimum SDK version and target SDK version are set in the `build.gradle` file, but are used to overwrite or set values in your `AndroidManifest.xml`

```java
compileSdkVersion 25
buildToolsVersion "25.0.0"

defaultConfig {
    applicationId "com.bignerdranch.android.geoquiz"
    minSdkVersion 19
    targetSdkVersion 25
    ...
}
``` 

#### Minimum SDK Version
* The `minSdkVersion` value is a hard floor between which the OS should refuse to install the app 

#### Target SDK Version
* The `targetSdkVersion` value tells Android which API level your app is _designed_ to run on
* Most often this will be the latest Android release
* Check developer.android.com/reference/android/os/Build.VERSION_CODES.html to see any significant changes between versions 
* Not increasing the target SDK when a new version of Android is released ensures that your app will still run with the appearance and behavior of the targeted version on which it worked well

#### Compile SDK version
* `compileSdkVersion` or _build target_: This is not used to update the `AndroidManifest.xml` file, it is private information between you and the compiler
* Specifies which version to use when building your own code
* When Android Studio is looking to find the classes and methods you refer to in your imports, the build target determines which SDK version it checks against
* The best choice for a build target is the latest API level


You can modify the minimum SDK version, target SDK version, and compile SDK version in your `build.gradle` file, but you must sync your project with the Gradle changes before they will be reflected

___

# UI Fragments and the Fragment Manager
## Fragments

* A _fragment_ is a controller object that an activity can deputize to perform tasks. 
* Most commonly, the task is managing a UI. The UI can be an entire screen or just one part of the screen
* A fragment managing a UI is known as a UI fragment
* A UI fragment has a view of its own that is inflated from a layout file
* The fragment's view contains the interesting UI elements that the user wants to see and interact with
* Using UI fragments separates the UI of your app into building blocks. Working with individual blocks, it is easy to build tab interfaces, tack on animated sidebars and more

### Two types of fragments
* **Native fragments**: Built into the device that the user runs your app on
  * import: `android.app`  
* **Support fragments**: Built into a library that you include in your application
  * import: `android.support.v4.app`

### Adding dependencies in Gradle
* To use a dependency, it must be included in your list of dependencies 
* Gradle allows for the specification of dependencies that you have not copied into your project
* When your app is compiled, Gradle will find, download, and include the dependencies for you 
* Ensure you sync your Gradle file to reflect any updates that you have made
* The dependencies use the Maven coordinates format: `groupId:artifactId:version`

  |groupId | artifactId | version 
  |--------------------|-------------|------
  |`com.android.support` |`appcompat-v7`|`27.1.1`
  * `groupId`: Unique identifier for a set of libraries available on the Maven repository
  * `artifactId`: The name of a specific library within the package
  * `version`: Represents the revision number of the library

### Hosting a UI Fragment
* To host a UI fragment, an activity must:
  * define a spot in its layout for the fragment's view
  * manage the lifecycle of the fragment instance

### The fragment lifecycle
* One critical difference between the fragment lifecycle and the activity lifecycle is that **fragment lifecycle methods are called by the hosting activity, not the OS**
* The OS knows nothing about the fragments that an activity is using to manage things 
* Fragments are the activity's internal business 

#### Two approaches to hosting
* You have two options when it comes to hosting a UI fragment in an activity:
  * Add the fragment to the activity's _layout_ 
    * Known as a _layout fragment_
    * Straightforward but inflexible
    * If you add the fragment to the activity's layout, you hardwire the fragment and its view to the activity's view and cannot swap out that fragment during the activity's lifetime
  * Add the fragment to the activity's _code_ 
    * More complex - but it is the only way to have control at runtime over your fragments 
    * You determine when the fragment is added to the activity and what happens to it after that
    * You can remove the fragment, replace it with another, and then add the first fragment back again

### Creating a UI Fragment
* The steps to create a UI fragment are the same as those you followed to create an activity:
  1. Compose a UI by defining widgets in a layout file
  2. Create the class and set its view to be the layout that you defined
  3. Wire up the widgets inflated from the layout in code

#### Overriding Fragment.onCreate(Bundle)
* `Fragment.onCreate(Bundle)` is a public method while `Activity.onCreate(Bundle)` is protected
* Fragment lifecycle methods must be public, because they will be called by whatever activity is hosting the fragment


* In `Fragment.onCreate(Bundle)` you do not inflate the fragment's view, you create and configure the fragment's view in another fragment lifecycle method:
  * `public View onCreateView(Layoutinflater inflater, ViewGroup container, Bundle savedInstanceState)`

* This method is where you inflate the layout for the fragment's view and return the inflated **View** to the hosting activity
* **LayoutInflater** and **ViewGroup** parameters are necessary to inflate the layout, **Bundle** will contain data that this method can use to re-create the view from a saved state
* Within `onCreateView(...)`, you explicitly inflate the fragment's view by calling `LayoutInflater.inflate(int, ViewGroup, boolean)` and passing in the layout resource ID
* The second parameter is your view's parent, which is usually needed to configure the widgets properly.
* The third parameter tells the layout inflater whether to add the inflated view to the view's parent

#### Wiring widgets in a fragment

* Declare and assign any `View` variables you want to use in `onCreateView()`
* Getting references in `Fragment.onCreateView(...)` works nearly the same as in `Activity.onCreate(Bundle)`, the only difference is that you call `View.findViewById(int)` on the fragment's view.
* Setting listeners in a fragment works exactly the same as in an activity
___

## Adding a UI Fragment to the FragmentManager

The `FragmentManager` is responsible for managing your fragments and adding their views to the activity's view hierarchy

The `FragmentManager` handles two things:
  1. A list of fragments
  2. Back stack of fragment transactions

* Instantiate a `FragmentManager` object within the call to `onCreate(Bundle)` in your `Activity` class
  * `FragmentManager fm = getSupportFragmentManager();`
  * You call `getSupportFragmentManager()` because you are using the support library 

#### Fragment Transactions

* Fragment transactions are used to add, remove, attach, detach, or replace fragments in the fragment list
* The `FragmentManager` maintains a back stack of fragment transactions you can navigate

`FragmentManager.beginTransaction()` : Creates and returns an instance of `FragmentTransaction`
* `FragmentTransaction` class uses a _fluent interface_ 
  * methods that configure `FragmentTransaction` return a `FragmentTransaction` instead of `void`, which allows you to chain them together

* `FragmentManager.add(...)` method has two parameters:
  1. A container view ID
  2. And the fragment you wish to add
* A container view ID serves two purposes:
  1. It tells the `FragmentManager` where in the activity's view the fragment's view should appear
  2. It is used as a unique identifier for a fragment in the `FragmentManager`'s list

* When an activity is destroyed, it's `FragmentManager` saves out its list of fragments
* When the activity is re-created, the new `FragmentManager` retrieves the list and re-creates the listed fragments to make everything as it was before

___

## FragmentManager and the Fragment Lifecycle
* The `FragmentManager` of an activity is responsible for calling the lifecycle methods of the fragment in its list
* `onAttach(Context)` `onCreate(Bundle)` `onCreateView(...)` are called when you add the fragment to the `FragmentManager`, `onActivityCreated(Bundle)` is called after the hosting activity's `onCreate(Bundle)` nethod has been executed
* If you add a fragment while the activity is already resumed, the `FragmentManager` walks the fragment through whatever steps are necessary to get it caught up to the activity's state

1. `onAttach()`: Method is called first, even before `onCreate()`, letting us know that you fragment has been attached to an activity. You are passed the Activity that will host your fragment
2. `onCreateView()`: The system calls this callback when its time for the fragment to draw its UI for the first time. To draw a UI for the fragment, a View component must be returned from this method which is the root of the fragment's layout. We can return null if the fragment does not provide a UI 
3. `onViewCreated()`
4. `onActivityCreated()`
5. `onStart()`
6. `onPause()`
7. `onStop()`
8. `onDestroyView()`
9. `onDestroy()`

#### Application Architecture with Fragments
* Fragments are intended to encapsulate major components in a reusable way
* A good rule of thumb is to have no more than two or three fragments on the screen at a time 
* **_Always Use Fragments_**

#### Why Suppport Fragments are Superior
* Support fragments are superior because you can update the version of the support library in your application and ship a new version of your app at any time
* There are no significant downsides to using the support library's fragments
* The only downside is that it has a nonzero size and you need to include the library in your project

___

### RecyclerView, Adapter, and ViewHolder
_**RecyclerView**_
* `RecyclerView` is a subclass of `ViewGroup`. It displays a list of child `View` objects, one for each item in your list of items. Depending on the complexity of what you need to display, these child `View`s can be complex or very simple
* `RecyclerView` reuses views rather than removing them from memory

* `RecyclerView` requires a `LayoutManager` to work
* `RecyclerView` does not position items on the screen itself, it delegates that job to the `LayoutManager` 
  * `LinearLayoutManager`: position the items in the list vertically
  * `GridLayoutManager`: position the items in a grid



_**ViewHolder**_
* It does one thing: It holds onto a `View`
* It holds a reference to the entire `View` you pass into `super(view)`
_Typical `ViewHolder` subclass_
```java
public class ListRow extends RecyclerView.ViewHolder {
    public ImageView mThumbnail;

    public ListRow(View view) {
        super(view);

        mThumbnail = (ImageView) view.findViewById(R.id.thumbnail);
    }
}
```

_Typical usage of a `ViewHolder`_
```java
ListRow row = new ListRow(inflater.inflate(R.layout.list_row, parent, false));
View view = row.itemView;
ImageView thumbnailView = row.mThumbnail;
```
* Always be efficient in your `onBindViewHolder(...)`, otherwise your scroll animation will not display so smoothly

* A `RecyclerView` never creates `Views` by themselves. It always creates `ViewHolders` which bring their _itemViews_ along for the ride 

#### _**Adapters**_
* An adapter is a controller object that sits between the `RecyclerView` and the data set that the `RecyclerView` should display
* The adapter is responsible for:
  * creating the necessary **ViewHolders**
  * binding **ViewHolders** to data from the model layer
* Take data from the datasource and pass it onto the Layout Manager

#### Reloading a list
* `RecyclerView`'s `Adapter` needs to be informed that the data has changed (or may have changed) so that it can refetch the data and reload the list
* You can work with the `ActivityManager`'s back stack to reload the list at the right moment
* 

### Responding to Presses

___

## Singletons

* Singletons will exist across rotation and will exist as you move between activities and fragments in your application
* Singletons will be destroyed, along with all of their instance variables, as Android reclaims memory at some point after you switch out of an application 
* Not a long-term storage solution 
___

## Creating User Interfaces with Layouts and Widgets

### ConstraintLayout
* Instead of using nested layouts you add a series constraints to your layout
* A constraint is like a rubber band, it pulls two things toward each other
* To place views where you want them to go in a `ConstraintLayout`, give them constraints instead of dragging them around the screen
* View size setting types:
  * **fixed** (xdp): Specifies an explicit size (that will not change) for the view. The size is specified in `dp` units
  * **wrap content** (wrap_content): Assigns the view its "desired" size. For a `TextView`, this means that the size will be just big enough to show its contents
  * **any size** (0dp): Allows the view to stretch to meet the specified constraints

* All attributes that begin with `layout_` are known as _layout parameters_
* Layout parameters are directions to that widget's _parent_, not the widget itself
* They tell the parent layout how to arrange the child element within itself

#### Screen pixel density
* _Text Size_: Pixel height of the text on the device's screen
* _Margin_: Distances between views
* _Padding_: Distance between a view's outside edges and its content
* _Density-independent_ (dp): Typically use this for margins, padding, or anything else for which you would otherwise specify size with a pixel value. You get the same size regardless of screen density 
* _Scale-independent_ (sp): Density-independent pixels that also take into account the user's front size preference. You will almost always use `sp` to set display text size
* _pt, mm, in_ : These are scaled units, like `dp`, that allow you to specify interface sizes in points (1/72 of an inch), millimeters, or inches

#### Margins vs Padding
_Margin_
* Margin attributes are layout parameters, they determine the distance between widgets
* Given that a widget can only know about itself, margins must be the responsibility of the widget's parent

_Padding_
* Not a layout parameter
* Tells the widget how much bigger than its content it should draw itself

#### Styles, themes, and theme attributes
* A _style_ is an XML resource that contains attributes that describe how a widget should look and behave
* You can apply a style from the app's theme to a widget using a _theme attribute reference_ 
* A _theme_ is a collection of styles
* Structurally, a theme is itself a style resource whose attributes point to other style resources
* A _theme attribute reference_ tells Android's runtime resource manager: "Go the app's theme and find the attribute named X. This attribute points to another style resource, put the value of the resource here"
___

### Using Fragment Arguments 

##### Starting an Activity from a Fragment
* You call the `Fragment.startActivity(Intent)` method, which calls the corresponding Activity method behind the scenes

##### Retrieving an extra
* There are two ways a fragment can access data in its activity's intent:
  * An easy, direct shortcut and a complex, flexible implementation

##### The downside to direct retrieval
* Having the fragment access the intent that belongs to the hosting activity makes for simple code, but it costs the encapsulation of the fragment

#### Fragment Arguments
* Every fragment instance can have a `Bundle` object attached to it
* This bundle contains key-value pairs that work just like the intent extras of an `Activity` 
* Each pair is known as an _argument_ 

1. To create fragment arguments, you first create a `Bundle` object
2. Next, you use type-specific "put" methods of `Bundle` (similar to those of `Intent`) to add arguments to the bundle: 

```java

Bundle args = new Bundle();
    args.putSerializable(ARG_MY_OBJECT, myObject);
    args.putInt(ARG_MY_INT, myInt);
    args.putCharSequence(ARG_MY_STRING, myString);

```
#### Attaching arguments to a fragment
* To attach the arguments bundle to a fragment, you call `Fragment.setArguments(Bundle)`
* Attaching arguments to a fragment must be done after the fragment is created but before it is added to an activity
  * Android programmers follow a convention of adding a static method named `newInstance()` to the `Fragment` class. This method creates the fragment instance and bundles up and sets its arguments

#### Retrieving arguments
* When a fragment needs to access its arguments, it calls the `Fragment` method `getArguments` and then one of the type specific "get" methods of `Bundle`

#### Getting Results with Fragments
* `Fragment.startActivityForResult(Intent, int)` is similar to the `Activity` method with the same name. It includes some additional code to route the result to your fragment from its host activity
* Returning results from a fragment is a bit different. A fragment can receive a result from an activity, but it cannot have its own result
* Only activities have results
* `Fragment` has its own `startActivityForResult(...)` and `onActivityResult(...)` methods, it does not have any `setResult(...) methods`
* Instead, you tell the _host activity_ to return a value:

```java
public class CrimeFragment extends Fragment {
  ...
  public void returnResult() {
    getActivity().setResult(Activity.RESULT_OK, null);
  }
}
``` 

#### Why Use Fragment Arguments?
* Why not just set an instance variable on the Fragment when it is created?*
  * When the OS re-creates your fragment - either across a configuation change or when the user has switched out of your app and the OS reclaims memory - all of your instance variables will be lost
  * There is no way to cheat low-memory death, your app's memory will be reclaimed eventually
* Two methods of persisting your arguments:
  1. Use the saved instance state mechanism. You can store the value as a normal instance variable, save the ID in `onSaveInstanceState(Bundle)` and snag it from the `Bundle` in `onCreate(Bundle)`. This will work in all situations but it is hard to maintain. You might come back in a few eyars and add another argument, forgetting to save it in `onSaveInstanceState(Bundle)`. This is less explicit
  2. Android developers prefer the fragment arguments solution because it is very explicity and clear in its intentions. In a few years, you will come back and know that the crime ID is an argument and is safely shuttled along to new instances of this fragment. If you add another argument, you will know to stash it in the arguments bundle


  


